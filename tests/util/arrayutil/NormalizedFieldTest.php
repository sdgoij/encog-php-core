<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\test\util\arrayutil;

use encog\EncogError;
use encog\mathutil\Equilateral;
use encog\util\arrayutil\ClassItem;
use encog\util\arrayutil\NormalizationAction;
use encog\util\arrayutil\NormalizedField;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TypeError;

class NormalizedFieldTest extends TestCase {
	public function testCreateNamedAction() {
		$field = NormalizedField::createNamedAction("dumdum", new NormalizationAction(NormalizationAction::PassThrough));
		$this->assertEquals("dumdum", $field->getName());
		$this->assertEquals(new NormalizationAction(NormalizationAction::PassThrough), $field->getAction());
		$this->assertEquals(0.0, $field->getNormalizedHigh());
		$this->assertEquals(0.0, $field->getNormalizedLow());
		$this->assertEquals(0.0, $field->getActualHigh());
		$this->assertEquals(0.0, $field->getActualLow());
	}

	public function testInit() {
		$eqAction = new NormalizationAction(NormalizationAction::Equilateral);
		$oneOfAction = new NormalizationAction(NormalizationAction::OneOf);
		$classes = [
			new ClassItem("a", 0),
			new ClassItem("b", 1),
			new ClassItem("c", 2)
		];

		$field = new NormalizedField(1.0, 0.0);
		$field->setAction($eqAction);

		$this->assertEquals(-1, $field->lookup("a"));
		$this->assertEquals(-1, $field->lookup("b"));
		$this->assertEquals(-1, $field->lookup("c"));

		$field->setClasses(...$classes);
		$field->init();

		$this->assertEquals($classes, $field->getClasses());
		$this->assertEquals(0, $field->lookup("a"));
		$this->assertEquals(1, $field->lookup("b"));
		$this->assertEquals(2, $field->lookup("c"));
		$this->assertNotNull($field->getEq());

		try {
			$field = new NormalizedField(1.0, 0.0);
			$field->setAction($oneOfAction);

			$this->assertEquals(-1, $field->lookup("a"));
			$this->assertEquals(-1, $field->lookup("b"));
			$this->assertEquals(-1, $field->lookup("c"));

			$field->setClasses(...$classes);
			$field->init();

			$this->assertEquals($classes, $field->getClasses());
			$this->assertEquals(0, $field->lookup("a"));
			$this->assertEquals(1, $field->lookup("b"));
			$this->assertEquals(2, $field->lookup("c"));
			$this->assertNull($field->getEq());
		} catch (TypeError $e) {
			$this->assertStringMatchesFormat(
				"%s::getEq()%s encog\mathutil\Equilateral, null returned",
				$e->getMessage()
			);
		}

		$this->expectException(EncogError::class);
		$this->expectExceptionMessage("There must be at least three classes to make use of equilateral normalization.");
		NormalizedField::createNamedAction("abc", $eqAction)->init();
	}

	public function testToString() {
		$format = "[NormalizedField name=%s, actualHigh=%f, actualLow=%f]";

		$this->assertEquals(sprintf($format, "abc", 0.0, 0.0),
			NormalizedField::createNamedAction("abc", new NormalizationAction(NormalizationAction::Ignore)));
		$this->assertEquals(sprintf($format, "", 3.0, -2.0), new NormalizedField(1, 0, 3, -2));
		$this->assertEquals(sprintf($format, "", INF, -INF), new NormalizedField(1, 0));
	}

	public function testAnalyze() {
		$values = range(-10, 10);
		shuffle($values);

		$field = new NormalizedField(1.0, 0.0);
		$field->setName("xyz");

		$this->assertEquals(1.0, $field->getNormalizedHigh());
		$this->assertEquals(0.0, $field->getNormalizedLow());
		$this->assertEquals(-INF, $field->getActualHigh());
		$this->assertEquals(INF, $field->getActualLow());
		$this->assertEquals("xyz", $field->getName());

		foreach ($values as $value) {
			$field->analyze($value);
		}

		$this->assertEquals(10, $field->getActualHigh());
		$this->assertEquals(-10, $field->getActualLow());

		$field->setNormalizedHigh(1);
		$field->setNormalizedLow(-1);
		$field->setActualHigh(11);
		$field->setActualLow(-11);

		foreach ($values as $value) {
			$field->analyze($value);
		}

		$this->assertEquals(1, $field->getNormalizedHigh());
		$this->assertEquals(-1, $field->getNormalizedLow());
		$this->assertEquals(11, $field->getActualHigh());
		$this->assertEquals(-11, $field->getActualLow());
	}

	public function testNormalize() {
		$field = new NormalizedField(1.0, 0.0, 10.0, 0.0);

		$this->assertSame(1.0, $field->normalize(11));
		$this->assertSame(0.0, $field->normalize(-1));

		for ($i = 0; $i <= 10; $i++) {
			$this->assertEquals($i/10, $field->normalize($i));
		}

		$this->expectException(LogicException::class);
		$this->expectExceptionMessage("here be dragons");
		(new NormalizedField(0,0,0,0))->normalize(0);
	}

	public function testDenormalize() {
		$field = new NormalizedField(1.0, 0.0, 10.0, 0.0);

		$this->assertSame(10.0, $field->denormalize(1.0));
		$this->assertSame(0.0, $field->denormalize(0.0));

		for ($i = 0; $i <= 10; $i++) {
			$this->assertEquals($i, $field->denormalize($i/10));
		}
	}

	public function testRoundTrip() {
		$field = new NormalizedField(1.0, 0.0, 10.0, 0.0);
		foreach (range(0.0, 10.0, 0.1) as $value) {
			$this->assertEquals($value, $field->denormalize($field->normalize($value)));
		}
	}

	public function testIsClassify() {
		/** @var NormalizationAction|MockObject $action */
		$action = $this->createMock(NormalizationAction::class);
		$action->expects($this->once())
			->method("isClassify")
			->willReturn(true);

		$field = new NormalizedField(1, 0);
		$field->setAction($action);

		$this->assertTrue($field->isClassify());
	}

	public function testGetEq() {
		$eq = new Equilateral(3, 1.0, 0.0);
		$field = new NormalizedField(1.0, 0.0);
		$field->setEq($eq);
		$this->assertSame($eq, $field->getEq());

		$this->expectException(TypeError::class);
		(new NormalizedField(1.0, 0.0))->getEq();
	}
}
