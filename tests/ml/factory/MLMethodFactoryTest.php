<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\test\ml\factory;

use encog\Encog;
use encog\EncogError;
use encog\ml\factory\MLMethodFactory;
use encog\ml\MLMethod;
use PHPUnit\Framework\TestCase;

class MLMethodFactoryTest extends TestCase {
	public function testCreateMethod() {
		$factory = new MLMethodFactory();
		$this->assertInstanceOf(MLMethod::class, $factory->create("TEST_OK", "?:b->?b", 2, 1));
		$this->expectException(EncogError::class);
		$this->expectExceptionMessage("Unknown method type: TEST_FAIL");
		$factory->create("TEST_FAIL", "", 2, 1);
	}
	public function setUp(): void {
		Encog::getInstance()->reset()->registerPlugin(new DummyPlugin());
	}
	public function tearDown(): void {
		Encog::getInstance()->reset();
		parent::tearDown();
	}
}
