<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\test\neural\som\training\basic\neighborhood;

use encog\neural\som\training\basic\neighborhood\Single;
use PHPUnit\Framework\TestCase;

class SingleTest extends TestCase {
	/** @dataProvider calculateParamProvider */
	public function testCalculate() {
		list ($current, $best, $expected) = func_get_args();
		$this->assertSame($expected, (new Single())->calculate($current, $best));
	}

	public function calculateParamProvider(): array {
		return [
			[0, 0, 1.0],
			[0, 1, 0.0],
			[-1, -1, 1.0],
			[-1, 0, 0.0],
			[M_PI, M_PI, 1.0],
			[M_PI, M_E, 0.0],
		];
	}

	public function testGetRadius() {
		$this->assertSame(1.0, (new Single())->getRadius());
	}
}
