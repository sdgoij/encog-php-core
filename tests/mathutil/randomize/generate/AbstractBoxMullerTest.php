<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\test\mathutil\randomize\generate;

use encog\mathutil\randomize\generate\AbstractBoxMuller;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AbstractBoxMullerTest extends TestCase {
	public function testNextGaussian() {
		/** @var AbstractBoxMuller|MockObject $generator */
		$generator = $this->getMockForAbstractClass(AbstractBoxMuller::class);
		$generator->expects($this->exactly(2))->method("next")->willReturn(0.3, 0.4);

		$this->assertEquals(-1.6047120177448, $generator->nextGaussian());
		$this->assertEquals(-0.8023560088724, $generator->nextGaussian());
	}
}
