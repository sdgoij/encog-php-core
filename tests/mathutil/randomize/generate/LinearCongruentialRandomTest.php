<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\test\mathutil\randomize\generate;

use encog\mathutil\randomize\generate\LinearCongruentialRandom;
use PHPUnit\Framework\TestCase;

class LinearCongruentialRandomTest extends TestCase {
	public function testCreateLinearCongruentialRandomNumberGenerator() {
		$g = new LinearCongruentialRandom(1, 2, 3, 4);
		$this->assertEquals(1, $g->getSeed());
		$this->assertEquals(2, $g->getModulus());
		$this->assertEquals(3, $g->getMultiplier());
		$this->assertEquals(4, $g->getIncrement());
	}
}
