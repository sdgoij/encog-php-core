# Introduction

This simple "application" demonstrates the usage of a BasicNetwork trained on the [MNIST](http://yann.lecun.com/exdb/mnist/) dataset.

## Run

```php -S 0.0.0.0:1337 -t './' 'example.php'```

## TODO

 - GUI, make it look nice. And present results, instead of just printing to browser console
 - Refactor to ReactPHP/Ratchet application
 - Use WebSocket for client-server communication
