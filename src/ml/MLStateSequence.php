<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\ml;

use encog\ml\data\MLDataSet;

/**
 * A state sequence ML method, for example a Hidden Markov Model.
 */
interface MLStateSequence extends MLMethod {
	public function getStatesForSequence(MLDataSet $seq): array;
	public function probability(MLDataSet $seq, array $states = null): float;
}
