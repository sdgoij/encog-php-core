<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\ml;

/**
 * Defines a Machine Learning Method that holds properties.
 */
interface MLProperties extends MLMethod {
	public function getProperties(): array;
	public function getPropertyDouble(string $name): float;
	public function getPropertyLong(string $name): int;
	public function getPropertyString(string $name): string;
	public function setProperty(string $name, $value);
	public function updateProperties();
}
