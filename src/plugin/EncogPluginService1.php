<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\plugin;

use encog\engine\network\activation\ActivationFunction;
use encog\ml\data\MLDataSet;
use encog\ml\MLMethod;
use encog\ml\train\MLTrain;

/**
 * A service plugin provides services, such as the creation of activation
 * functions, machine learning methods and training methods.
 */
interface EncogPluginService1 extends EncogPluginBase {
	public function createActivationFunction(string $name): ActivationFunction;
	public function createMethod(string $type, string $arch, int $input, int $output): MLMethod;
	public function createTraining(MLMethod $method, MLDataSet $training, string $type, string $args): MLTrain;
}
