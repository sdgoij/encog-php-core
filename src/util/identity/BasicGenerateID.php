<?php
/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace encog\util\identity;

/**
 * Used to generate a unique id.
 */
class BasicGenerateID implements GenerateID {
	public function __construct() {
		$this->current = 1;
	}

	public function getCurrentID(): int {
		return $this->current;
	}

	public function setCurrentID(int $id) {
		$this->current = $id;
	}

	public function generate(): int {
		return $this->current++;
	}

	/** @var int */
	private $current;
}
